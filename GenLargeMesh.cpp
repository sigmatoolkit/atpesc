/** @example GenLargeMesh.cpp \n
 * \brief Create a large structured mesh, partitioned \n
 * <b>To run</b>:  ./GenLargeMesh \n
 *
 *  It shows how to create a mesh on the fly, on multiple processors
 *  Each processor will create its version of a block mesh, partitioned
 *  as AxBxC blocks. Each block will be with blockSizeElement^3 hexahedrons, and will get a
 *  different PARALLEL_PARTITION tag
 *
 *  The number of tasks will be MxNxK, and it must match the mpi size
 *  Each task p will generate its mesh at location (m,n,k) , and it is
 *  lexicographic ordering: rank = m + n * M + k * M * N
 *
 *  By default M=1, N=1, K=1, so by default it should be launched on 1 proc
 *  By default, blockSizeElement is 4, and A=2, B=2, C=2, so each task will generate locally
 *    blockSizeElement^3 x A x B x C hexahedrons (value = 64x8 = 512 hexas, in 8 partitions)
 *   (if -t, multiple by 6 for total number of cells/tets)
 *  The total number of partitions will be A*B*C*M*N*K (default 8)
 *
 *  Each part in partition will get a proper tag, and the numbering is in
 *  lexicographic ordering; x direction varies first, then y , then z.
 *  The same principle is used for global id numbering of the nodes and cells.
 *  (x varies first)
 *
 *  The vertices will get a proper global id, which will be used to resolve the
 *  shared entities
 *  The output will be written in parallel, and we will try sizes as big as we can
 *  (up to a billion vertices, because we use int for global ids)
 *
 *  Within each partition, the hexas entitiy handles will be contiguous, and also the
 *  vertices; The global id will be determined easily, for a vertex, but the entity
 *  handle space will be more interesting to handle, within a partition (we want
 *  contiguous handles within a partition). After merging the vertices, some fragmentation
 *  will occur in the vertices handle space, within each partition.
 *
 *  to run: ./GenLargeMesh
 *
 *  when launched on more procs, you have to make sure
 *  num procs = M*N*K
 *
 *  so you can launch with
 *  mpiexec -np 8 ./GenLargeMesh -M 2 -N 2 -K 2
 *
 *  We also added -q option; it works now only for hexa mesh, it will generate
 *  quadratic hex27 elements
 *
 *  -t option will generate tetrahedrons instead of hexahedra. Each hexahedra is
 *  decomposed into 6 tetrahedrons.
 *
 *  -f option will also generate all edges and faces in the model.
 *  -w will use a newer merging method locally. Merging is necessary to merge
 *  vertices on the local task, and the new method does not use a searching tree,
 *  but rather the global id set on the vertices in a consistent manner
 *
 *  -d and -i options can be used to add some artificial tags on the model ;
 *  you can have multiple -d and -i options; -i <tag_name> will set an integer
 *  tag with name tag_name on the vertices; -d < tag_name2> will generate
 *  double tags on cells (3d elements). You can have multiple tags , like
 *  -i tag1 -i tag2 -i tag3 -d tag4
 *
 *  -x, -y, -z options will control the geometric dimensions of the final mesh, in
 *  x, y and z directions.
 *
 *  -o <out_file> controls the name of the output file; it needs to have extension h5m,
 *   because the file is written in parallel.
 *
 *   -k will keep the edges and faces that are generated as part of resolving shared entities
 *   (by default these edges and faces are removed) ; when -f option is used, the
 *   -k option is enabled too (so no faces and edges are deleted)
 *
 */

#include "moab/Core.hpp"
#include "moab/ProgOptions.hpp"
#include "moab/ParallelComm.hpp"
#include "moab/CN.hpp"
#include "moab/ReadUtilIface.hpp"
#include "moab/MergeMesh.hpp"
#include "MBExamples.hpp"
#include <time.h>

#include <iostream>
#include <vector>

using namespace moab;
using namespace std;

typedef struct {
  // options
  int A,B,C,M,N,K,dim;
  int blockSizeVertex, blockSizeElement;              // Number of element blocks per partition
  double xsize,ysize,zsize; // the physical size of the domain
  bool newMergeMethod,keep_skins,simplex,adjEnts;
  vector<string> intTagNames;
  string firstIntTag;
  vector<string> doubleTagNames;
  string firstDoubleTag;
  string outFileName;

  // compute params
  int q;
  //factor;
  double dx,dy,dz;
  int NX,NY,nex,ney;
  int xstride,ystride,zstride;
} UserContext;

int process_options(int argc, char **argv, UserContext& user)
{
  ProgOptions opts;

  /* Initialize all user data */
  user.A=user.B=user.C=1;
  user.M=user.N=user.K=1;
  user.blockSizeElement=4;
  user.xsize=user.ysize=user.zsize=1.0;
  user.dim=3;
  user.newMergeMethod=user.keep_skins=user.simplex=user.adjEnts=false;
  user.outFileName="output/GenLargeMesh.h5m";
  /* Done */

  opts.addOpt<int>(std::string("topology,t"),
      std::string("Topological dimension of the mesh to be generated (default=3)"), &user.dim);

  opts.addOpt<int>(std::string("blockSizeElement,b"),
      std::string("Block size of mesh (default=4)"), &user.blockSizeElement);

  opts.addOpt<int>(std::string("xproc,M"),
      std::string("Number of processors in x dir (default=1)"), &user.M);
  opts.addOpt<int>(std::string("yproc,N"),
      std::string("Number of processors in y dir (default=1)"), &user.N);
  opts.addOpt<int>(std::string("zproc,K"),
      std::string("Number of processors in z dir (default=1)"), &user.K);

  opts.addOpt<int>(std::string("xblocks,A"),
      std::string("Number of blocks on a task in x dir (default=2)"), &user.A);
  opts.addOpt<int>(std::string("yblocks,B"),
      std::string("Number of blocks on a task in y dir (default=2)"), &user.B);
  opts.addOpt<int>(std::string("zblocks,C"),
      std::string("Number of blocks on a task in x dir (default=2)"), &user.C);

  opts.addOpt<double>(std::string("xsize,x"),
        std::string("Total size in x direction (default=1.)"), &user.xsize);
  opts.addOpt<double>(std::string("ysize,y"),
        std::string("Total size in y direction (default=1.)"), &user.ysize);
  opts.addOpt<double>(std::string("zsize,z"),
        std::string("Total size in z direction (default=1.)"), &user.zsize);

  opts.addOpt<void>("newMerge,w", "Use new merging method", &user.newMergeMethod);
  opts.addOpt<void>("keep_skins,k", "Keep skins with shared entities", &user.keep_skins);
  opts.addOpt<void>("simplices,s", "Generate simplices", &user.simplex);
  opts.addOpt<void>("faces_edges,f", "Create all faces and edges", &user.adjEnts);

  opts.addOpt<string>(std::string("int_tag_cells,i"), string("Add integer tag on cells"), &user.firstIntTag);
  opts.addOpt<string>(std::string("double_tag_verts,d"), string("Add double tag on vertices"), &user.firstDoubleTag);

  opts.addOpt<std::string> ("outFile,o", "Specify the output file name string (default GenLargeMesh.h5m)", &user.outFileName );

  opts.parseCommandLine(argc, argv);

  opts.getOptAllArgs("int_tag_cells,i", user.intTagNames);
  opts.getOptAllArgs("double_tag_verts,d", user.doubleTagNames);

  return 0;
}

ErrorCode GenerateVertices(Interface * mbImpl, ReadUtilIface *iface, UserContext& user, int m, int n, int k,
    int a, int b, int c, Tag& global_id_tag, vector<Tag>& doubleTags, EntityHandle& startv)
{
  // we will generate (q*block+1)^3 vertices, and block^3 hexas; q is 1 for linear, 2 for quadratic
  // the global id of the vertices will come from m, n, k, a, b, c
  // x will vary from  m*A*q*block + a*q*block to m*A*q*block+(a+1)*q*block etc;
  int num_nodes = pow(user.blockSizeVertex,user.dim);
  vector<double*> arrays;

  ErrorCode rval = iface->get_node_coords(3, num_nodes, 0, startv, arrays);CHKERR(rval, "Can't get node coords.");

  // will start with the lower corner:
  int x = m * user.A * user.q * user.blockSizeElement + a * user.q * user.blockSizeElement;
  int y = n * user.B * user.q * user.blockSizeElement + b * user.q * user.blockSizeElement;
  int z = k * user.C * user.q * user.blockSizeElement + c * user.q * user.blockSizeElement;
  int ix = 0;
  vector<int> gids(num_nodes);
  Range verts(startv, startv + num_nodes - 1);
  for (int kk = 0; kk < (user.dim>2?user.blockSizeVertex:1); kk++) {
    for (int jj = 0; jj < (user.dim>1?user.blockSizeVertex:1); jj++) {
      for (int ii = 0; ii < user.blockSizeVertex; ii++) {
        /* set coordinates for the vertices */
        arrays[0][ix] = (x + ii) * user.dx;
        arrays[1][ix] = (y + jj) * user.dy;
        arrays[2][ix] = (z + kk) * user.dz;
        /* set the global ID */
        gids[ix] = 1 + (x + ii) + (y + jj) * user.NX + (z + kk) * (user.NX * user.NY);
        // set int tags, some nice values?
        EntityHandle v = startv + ix;
        for (size_t i = 0; i < doubleTags.size(); i++) {
          double dtagval=0.0;
          switch(user.dim) {
            case 1:
              dtagval = ((i+1)*gids[ix]) * cos(PI*arrays[0][ix]);
              break;
            case 2:
              dtagval = ((i+1)*gids[ix]) * cos(PI*arrays[0][ix])*cos(PI*arrays[1][ix]);
              break;
            case 3:
            default:
              dtagval = ((i+1)*gids[ix]) * cos(PI*arrays[0][ix])*cos(PI*arrays[1][ix])*cos(PI*arrays[2][ix]);
              break;
          }
          mbImpl->tag_set_data(doubleTags[i], &v, 1, &dtagval);
        }
        ix++;
      }
    }
  }
  mbImpl->tag_set_data(global_id_tag, verts, &gids[0]);
  return MB_SUCCESS;
}


int SetTensorElementConnectivity_Private(UserContext& user, int offset, int corner, std::vector<int>& subent_conn, moab::EntityHandle *connectivity)
{
  // subent_conn.resize(pow(2,dim))
  switch(user.dim) {
    case 1:
      subent_conn.resize(2);
      moab::CN::SubEntityVertexIndices(MBEDGE, 1, 0, subent_conn.data());
      connectivity[offset+subent_conn[0]] = corner;
      connectivity[offset+subent_conn[1]] = corner + 1;
      break;
    case 2:
      subent_conn.resize(4);
      moab::CN::SubEntityVertexIndices(MBQUAD, 2, 0, subent_conn.data());
      connectivity[offset+subent_conn[0]] = corner;
      connectivity[offset+subent_conn[1]] = corner + 1;
      connectivity[offset+subent_conn[2]] = corner + 1 + user.ystride;
      connectivity[offset+subent_conn[3]] = corner + user.ystride;
      break;
    case 3:
    default:
      subent_conn.resize(8);
      moab::CN::SubEntityVertexIndices(MBHEX, 3, 0, subent_conn.data());
      connectivity[offset+subent_conn[0]] = corner;
      connectivity[offset+subent_conn[1]] = corner + 1;
      connectivity[offset+subent_conn[2]] = corner + 1 + user.ystride;
      connectivity[offset+subent_conn[3]] = corner + user.ystride;
      connectivity[offset+subent_conn[4]] = corner + user.zstride;
      connectivity[offset+subent_conn[5]] = corner + 1 + user.zstride;
      connectivity[offset+subent_conn[6]] = corner + 1 + user.ystride + user.zstride;
      connectivity[offset+subent_conn[7]] = corner + user.ystride + user.zstride;
      break;
  }
  return subent_conn.size();  
}


int SetSimplexElementConnectivity_Private(UserContext& user, int subelem, int offset, int corner, std::vector<int>& subent_conn, moab::EntityHandle *connectivity)
{
  switch(user.dim) {
    case 1:
      subent_conn.resize(2);  /* only linear EDGE supported now */
      moab::CN::SubEntityVertexIndices(MBEDGE, 1, 0, subent_conn.data());
      connectivity[offset+subent_conn[0]] = corner;
      connectivity[offset+subent_conn[1]] = corner+1;
      break;
    case 2:
      subent_conn.resize(3);  /* only linear TRI supported */
      moab::CN::SubEntityVertexIndices(MBTRI, 2, 0, subent_conn.data());
      if (subelem) { /* 0 1 2 of a QUAD */
        connectivity[offset+subent_conn[0]] = corner;
        connectivity[offset+subent_conn[1]] = corner + 1;
        connectivity[offset+subent_conn[2]] = corner + 1 + user.ystride;
      }
      else {        /* 2 3 0 of a QUAD */
        connectivity[offset+subent_conn[0]] = corner + 1 + user.ystride;
        connectivity[offset+subent_conn[1]] = corner + user.ystride;
        connectivity[offset+subent_conn[2]] = corner;
      }
      break;
    case 3:
    default:
      subent_conn.resize(4);  /* only linear TET supported */
      moab::CN::SubEntityVertexIndices(MBTET, 3, 0, subent_conn.data());
      switch(subelem) {
        case 0: /* 0 1 2 5 of a HEX */
          connectivity[offset+subent_conn[0]] = corner;
          connectivity[offset+subent_conn[1]] = corner + 1;
          connectivity[offset+subent_conn[2]] = corner + 1 + user.ystride;
          connectivity[offset+subent_conn[3]] = corner + 1 + user.zstride;
          break;
        case 1: /* 0 2 7 5 of a HEX */
          connectivity[offset+subent_conn[0]] = corner;
          connectivity[offset+subent_conn[1]] = corner + 1 + user.ystride;
          connectivity[offset+subent_conn[2]] = corner + user.ystride + user.zstride;
          connectivity[offset+subent_conn[3]] = corner + 1 + user.zstride;
          break;
        case 2: /* 0 2 3 7 of a HEX */
          connectivity[offset+subent_conn[0]] = corner;
          connectivity[offset+subent_conn[1]] = corner + 1 + user.ystride;
          connectivity[offset+subent_conn[2]] = corner + user.ystride;
          connectivity[offset+subent_conn[3]] = corner + user.ystride + user.zstride;
          break;
        case 3: /* 0 5 7 4 of a HEX */
          connectivity[offset+subent_conn[0]] = corner;
          connectivity[offset+subent_conn[1]] = corner + 1 + user.zstride;
          connectivity[offset+subent_conn[2]] = corner + user.ystride + user.zstride;
          connectivity[offset+subent_conn[3]] = corner + user.zstride;
          break;
        case 4: /* 2 7 5 6 of a HEX */
          connectivity[offset+subent_conn[0]] = corner + 1 + user.ystride;
          connectivity[offset+subent_conn[1]] = corner + user.ystride + user.zstride;
          connectivity[offset+subent_conn[2]] = corner + 1 + user.zstride;
          connectivity[offset+subent_conn[3]] = corner + 1 + user.ystride + user.zstride;
          break;
      }
      break;
  }
  return subent_conn.size();
}


std::pair<int,int> SetElementConnectivity(UserContext& user, int offset, int corner, moab::EntityHandle *connectivity)
{
  int vcount=0;
  std::vector<int> subent_conn;  /* only linear edge, tri, tet supported now */
  subent_conn.reserve(27);
  int m,subelem;
  if (user.simplex) {
    subelem=(user.dim==1 ? 1 : (user.dim==2 ? 2 : 5));
    for (m=0; m<subelem; m++) {
      vcount=SetSimplexElementConnectivity_Private(user, m, offset, corner, subent_conn, connectivity);
      offset+=vcount;
    }
  }
  else {
    subelem=1;
    vcount=SetTensorElementConnectivity_Private(user, offset, corner, subent_conn, connectivity);
  }
  return std::pair<int,int>(vcount*subelem,subelem);
}


ErrorCode GenerateElements(Interface* mbImpl, ReadUtilIface* iface, UserContext& user, int m, int n, int k,
    int a, int b, int c, Tag& global_id_tag, vector<Tag>& intTags, EntityHandle startv, Range& cells)
{
  ErrorCode rval;
  int num_hexas = pow(user.blockSizeElement,user.dim);
  int num_el = num_hexas;

  EntityHandle starte; // connectivity
  EntityHandle* conn;
  int num_v_per_elem;
  switch(user.dim) {
    case 1:
      num_v_per_elem = 2;
      rval = iface->get_element_connect(num_el, 2, MBEDGE, 0, starte, conn);CHKERR(rval, "Can't get EDGE2 element connectivity.");
      break;
    case 2:
      if (user.simplex) {
        num_v_per_elem = 3;
        num_el = num_hexas*2;
        rval = iface->get_element_connect(num_el, 3, MBTRI, 0, starte, conn);CHKERR(rval, "Can't get TRI3 element connectivity.");
      }
      else {
        num_v_per_elem = 4;
        rval = iface->get_element_connect(num_el, 4, MBQUAD, 0, starte, conn);CHKERR(rval, "Can't get QUAD4 element connectivity.");
      }
      break;
    case 3:
    default:
      if (user.simplex) {
        num_v_per_elem = 4;
        num_el = num_hexas*5;
        rval = iface->get_element_connect(num_el, 4, MBTET, 0, starte, conn);CHKERR(rval, "Can't get TET4 element connectivity.");
      }
      else {
        num_v_per_elem = 8;
        rval = iface->get_element_connect(num_el, 8, MBHEX, 0, starte, conn);CHKERR(rval, "Can't get HEX8 element connectivity.");
      }
      break;
  }

  Range tmp(starte, starte + num_el - 1); // should be elements
  // fill  cellsfactor
  int ix = 0;
  vector<int> gids(num_el);
  int ie = 0; // index now in the elements, for global ids
  // identify the elements at the lower corner, for their global ids
  int xe = m * user.A * user.blockSizeElement + a * user.blockSizeElement;
  int ye = (user.dim > 1 ? n * user.B * user.blockSizeElement + b * user.blockSizeElement : 0);
  int ze = (user.dim > 2 ? k * user.C * user.blockSizeElement + c * user.blockSizeElement : 0);
  /* create owned elements requested by user */
  for (int kk = 0; kk < (user.dim>2?user.blockSizeElement:1); kk++) {
    for (int jj = 0; jj < (user.dim>1?user.blockSizeElement:1); jj++) {
      for (int ii = 0; ii < user.blockSizeElement; ii++) {

        EntityHandle corner = startv + user.q * ii + user.q * jj * user.ystride + user.q * kk * user.zstride;

        std::pair<int,int> entoffset = SetElementConnectivity(user, ix, corner, conn);

        for (int j = 0; j < entoffset.second; j++) {
          EntityHandle eh = starte + ie + j;
          gids[ie+j] = 1 + ((xe + ii) + (ye + jj) * user.nex + (ze + kk) * (user.nex * user.ney));
          //gids[ie+j] = ie + j + ((xe + ii) + (ye + jj) * user.nex + (ze + kk) * (user.nex * user.ney)) ;
          //gids[ie+j] = 1 + ie;
          for (size_t i = 0; i < intTags.size(); i++) {
            int itagval = gids[ie+j]*(i+1);
            mbImpl->tag_set_data(intTags[i], &eh, 1, &itagval);
          }
          ie++;
        }

        ix += entoffset.first;
        //ie += entoffset.second;
      }
    }
  }
  if (user.adjEnts) {
    // we need to update adjacencies now, because some elements are new
    rval = iface->update_adjacencies(starte, num_el, num_v_per_elem, conn);CHKERR(rval, "Can't update adjacencies");
  }
  tmp.swap(cells);
  rval = mbImpl->tag_set_data(global_id_tag, cells, &gids[0]);CHKERR(rval, "Can't set global ids to elements.");
  return MB_SUCCESS;
}

int main(int argc, char **argv)
{
  UserContext user;
  int global_rank, global_size, ierr;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank( MPI_COMM_WORLD, &global_rank );
  MPI_Comm_size( MPI_COMM_WORLD, &global_size );

  if (process_options(argc, argv, user)) {
    dbgprint( "Invalid command line options. Aborting ..." );
    MPI_Finalize();
    return 1;
  }
  
  /* Lets check for some valid input */
  if (user.M * user.N * user.K != global_size) {
    dbgprint( "M*N*K=" << user.M * user.N * user.K << "  != global_size = " << global_size << "\n Invalid M, N, K parameters" );
    MPI_Finalize();
    return 1;
  }

  if (user.adjEnts)
    user.keep_skins = true; // do not delete anything

  // determine other global quantities for the mesh
  // used for nodes increments
  user.q = 1;
  // used for element increments
  //user.factor = (user.simplex)? 6 : 1;

  user.dx = user.xsize/(user.A*user.M*user.blockSizeElement*user.q);// distance between 2 nodes in x direction
  user.dy = (user.dim>1 ? user.ysize/(user.B*user.N*user.blockSizeElement*user.q) : 0.0);// distance between 2 nodes in y direction
  user.dz = (user.dim>2 ? user.zsize/(user.C*user.K*user.blockSizeElement*user.q) : 0.0);// distance between 2 nodes in z direction

  user.NX = (user.q * user.M * user.A * user.blockSizeElement + 1);
  user.NY = (user.dim>1 ? (user.q * user.N * user.B * user.blockSizeElement + 1) : 0);
  user.nex = user.M * user.A * user.blockSizeElement; // number of elements in x direction, used for global id on element
  user.ney = (user.dim>1 ? user.N * user.B * user.blockSizeElement : 0); // number of elements in y direction  ....
  // int NZ = ( K * C * blockSizeElement + 1); not used
  user.blockSizeVertex = user.q*user.blockSizeElement + 1;// used for vertices

  user.ystride = (user.dim>1 ? user.blockSizeVertex : 0);
  user.zstride = (user.dim>2 ? user.blockSizeVertex * user.blockSizeVertex : 0);

  switch(user.dim) {
   case 1:
     user.ysize=0.0;
     user.dy=0.0;
     user.ystride=0;
   case 2:
     user.zsize=0.0;
     user.dz=0.0;
     user.zstride=0;
    default:
     break;
  }

  Core* mbImpl = new Core();

  ReadUtilIface *iface;
  ErrorCode rval = mbImpl->query_interface(iface);CHKERR(rval, "Can't get reader interface");

  // determine m, n, k for processor rank
  int m=0, n=0, k=0;
  k = (user.dim>2?global_rank/(user.M*user.N):0);
  int leftover = global_rank%(user.M*user.N);
  n = leftover/user.M;
  m = leftover%user.M;
  if (global_rank==global_size-1)
    cout << "m, n, k for last rank:" << m << " " << n << " " << k << "\n";

  // so there are a total of M * A * blockSizeElement elements in x direction (so M * A * blockSizeElement + 1 verts in x direction)
  // so there are a total of N * B * blockSizeElement elements in y direction (so N * B * blockSizeElement + 1 verts in y direction)
  // so there are a total of K * C * blockSizeElement elements in z direction (so K * C * blockSizeElement + 1 verts in z direction)

  // there are ( M * A blockSizeElement )      *  ( N * B * blockSizeElement)      * (K * C * blockSizeElement )    hexas
  // there are ( M * A * blockSizeElement + 1) *  ( N * B * blockSizeElement + 1 ) * (K * C * blockSizeElement + 1) vertices
  // x is the first dimension that varies

  clock_t tt = clock();

  // generate the block at (a, b, c); it will represent a partition , it will get a partition tag
  Tag global_id_tag, part_tag;
  rval = mbImpl->tag_get_handle("GLOBAL_ID", 1, MB_TYPE_INTEGER,
               global_id_tag);CHKERR(rval, "Getting Tag handle failed");

  int dum_id=-1;
  rval = mbImpl->tag_get_handle("PARALLEL_PARTITION", 1, MB_TYPE_INTEGER,
                 part_tag, MB_TAG_CREAT|MB_TAG_SPARSE, &dum_id);CHKERR(rval, "Getting Tag handle failed");

  // create tags on vertices and cells, look in the list of options
  vector<Tag> intTags(user.intTagNames.size());
  vector<Tag> doubleTags(user.doubleTagNames.size());
  for (size_t i=0; i<user.intTagNames.size(); i++) {
    rval = mbImpl->tag_get_handle(user.intTagNames[i].c_str(), 1, MB_TYPE_INTEGER, intTags[i],
        MB_TAG_CREAT|MB_TAG_DENSE, &i);CHKERR(rval, "Can't create integer tag.");
  }
  for (size_t i=0; i<user.doubleTagNames.size(); i++) {
    double defval=(i+1)*100.;
    rval = mbImpl->tag_get_handle(user.doubleTagNames[i].c_str(), 1, MB_TYPE_DOUBLE, doubleTags[i],
        MB_TAG_CREAT|MB_TAG_DENSE, &defval);CHKERR(rval, "Can't create double tag.");
  }

  Range cells;
  for (int a=0; a<(user.dim>0?user.A:user.A); a++) {
    for (int b=0; b<(user.dim>1?user.B:1); b++) {
      for (int c=0; c<(user.dim>2?user.C:1); c++) {
        EntityHandle startv,part_set;
        rval = GenerateVertices(mbImpl, iface, user, m, n, k, a, b, c, global_id_tag, doubleTags, startv);CHKERR(rval, "Can't generate vertices");

        rval = GenerateElements(mbImpl, iface, user, m, n, k, a, b, c, global_id_tag, intTags, startv, cells);CHKERR(rval, "Can't generate elements");

        rval = mbImpl->create_meshset(MESHSET_SET, part_set);CHKERR(rval, "Can't create mesh set.");

        rval = mbImpl->add_entities(part_set, cells);CHKERR(rval, "Can't add entities to set.");
        // if needed, add all edges and faces
        if (user.adjEnts)
        {
          Range edges, faces;
          if (user.dim > 1) {
            rval = mbImpl->get_adjacencies(cells, 1, true, edges, Interface::UNION);CHKERR(rval, "Can't get edges");
            rval = mbImpl->add_entities(part_set, edges);CHKERR(rval, "Can't add edges to partition set.");
          }
          if (user.dim > 2) {
            rval = mbImpl->get_adjacencies(cells, 2, true, faces, Interface::UNION);CHKERR(rval, "Can't get faces");
            rval = mbImpl->add_entities(part_set, faces);CHKERR(rval, "Can't add faces to partition set.");
          }
        }

        int part_num;
        switch(user.dim) {
         case 1:
           part_num = a + m*user.A;
           break;
         case 2:
           part_num = a + m*user.A + (b + n*user.B)*(user.M*user.A);
           break;
         case 3:
         default:
           part_num = a + m*user.A + (b + n*user.B)*(user.M*user.A) + (c+k*user.C)*(user.M*user.A * user.N*user.B);
           break;
        }

        rval = mbImpl->tag_set_data(part_tag, &part_set, 1, &part_num);CHKERR(rval, "Can't set part tag on set");
      }
    }
  }

  if (0==global_rank) {
      dbgprint( "\n\nGenerate local mesh:  " << (clock() - tt) / (double) CLOCKS_PER_SEC << " seconds" << std::endl );
      tt = clock();
  }

  MergeMesh mm(mbImpl);
  Range all3dcells,verts;

  rval = mbImpl->get_entities_by_dimension(0, user.dim, all3dcells);CHKERR(rval, "Can't get all d-dimensional elements.");
  rval = mbImpl->get_entities_by_dimension(0, 0, verts);CHKERR(rval, "Can't get all vertices.");

  if (user.A*user.B*user.C!=1) { //  merge needed
    if (user.newMergeMethod) {
      rval = mm.merge_using_integer_tag( verts, global_id_tag);CHKERR(rval, "Can't merge with GLOBAL_ID tag");
    }
    else {
      rval = mm.merge_entities(all3dcells, 0.0001);CHKERR(rval, "Can't merge with coordinates");
    }
    if (0==global_rank) {
       dbgprint ( "Merge mesh locally:  " << (clock() - tt) / (double) CLOCKS_PER_SEC << " seconds" << std::endl );
       tt = clock();
    }
  }

  /* resolving shared entities between processors */
  if (global_size>1) {
    ParallelComm* pcomm = ParallelComm::get_pcomm(mbImpl, 0);
    if (pcomm==NULL) {
      pcomm = new ParallelComm( mbImpl, MPI_COMM_WORLD );
    }
    rval = pcomm->resolve_shared_ents( 0, all3dcells, user.dim, 0 );CHKERR(rval, "Can't resolve shared entities");

    if (0==global_rank) {
       dbgprint ( "Resolving shared entities:  " << ( (clock() - tt)/CLOCKS_PER_SEC ) << " seconds" << std::endl );
       tt = clock();
    }

    if (!user.keep_skins) { // default is to delete the 1- and 2-dimensional entities
      // delete all quads and edges
      Range toDelete;
      if (user.dim > 1) {
        rval =  mbImpl->get_entities_by_dimension(0, 1, toDelete);CHKERR(rval, "Can't get edges");
      }

      if (user.dim > 2) {
        rval = mbImpl->get_entities_by_dimension(0, 2, toDelete);CHKERR(rval, "Can't get faces");
      }

      rval = pcomm->delete_entities(toDelete) ;CHKERR(rval, "Can't delete entities");

      if (0==global_rank) {
        dbgprint ( "Delete edges and faces, and correct sharedEnts:  " << (clock() - tt) / (double) CLOCKS_PER_SEC << " seconds" << std::endl );
        tt = clock();
      }
    }
  }
  rval = mbImpl->write_file(user.outFileName.c_str(), 0, (global_size>1?";;PARALLEL=WRITE_PART":""));CHKERR(rval, "Can't write in parallel");

  if (0==global_rank) {
    dbgprint( "Write file " << user.outFileName << " in " << (clock() - tt) / (double) CLOCKS_PER_SEC << " seconds" << std::endl );
    tt = clock();
  }

  MPI_Initialized(&ierr);
  if (ierr)
    MPI_Finalize();
  return 0;
}

