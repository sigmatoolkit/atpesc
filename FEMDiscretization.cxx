/*
 * =====================================================================================
 *
 *       Filename:  FEMDiscretization.cxx
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/14/2016 02:00:43 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Vijay S. Mahadevan (vijaysm), mahadevan@anl.gov
 *        Company:  Argonne National Lab
 *
 * =====================================================================================
 */

#include "FEMDiscretization.hxx"

/* Utility functions */

static PetscReal determinant_mat_2x2 ( PetscReal inmat[2*2] )
{
  return  inmat[0]*inmat[3]-inmat[1]*inmat[2];
}

static PetscErrorCode invert_mat_2x2 (PetscReal *inmat, PetscReal *outmat, PetscReal *determinant)
{
  if (!inmat) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_POINTER,"Invalid input matrix specified for 2x2 inversion.");
  PetscReal det = determinant_mat_2x2(inmat);
  if (outmat) {
    outmat[0]= inmat[3]/det;
    outmat[1]=-inmat[1]/det;
    outmat[2]=-inmat[2]/det;
    outmat[3]= inmat[0]/det;
  }
  if (determinant) *determinant=det;
  PetscFunctionReturn(0);
}

static double determinant_mat_3x3 ( PetscReal inmat[3*3] )
{
  return   inmat[0]*(inmat[8]*inmat[4]-inmat[7]*inmat[5])
         - inmat[3]*(inmat[8]*inmat[1]-inmat[7]*inmat[2])
         + inmat[6]*(inmat[5]*inmat[1]-inmat[4]*inmat[2]);
}

static PetscErrorCode invert_mat_3x3 (PetscReal *inmat, PetscReal *outmat, PetscScalar *determinant)
{
  if (!inmat) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_POINTER,"Invalid input matrix specified for 3x3 inversion.");
  double det = determinant_mat_3x3(inmat);
  if (outmat) {
    outmat[0]= (inmat[8]*inmat[4]-inmat[7]*inmat[5])/det;
    outmat[1]=-(inmat[8]*inmat[1]-inmat[7]*inmat[2])/det;
    outmat[2]= (inmat[5]*inmat[1]-inmat[4]*inmat[2])/det;
    outmat[3]=-(inmat[8]*inmat[3]-inmat[6]*inmat[5])/det;
    outmat[4]= (inmat[8]*inmat[0]-inmat[6]*inmat[2])/det;
    outmat[5]=-(inmat[5]*inmat[0]-inmat[3]*inmat[2])/det;
    outmat[6]= (inmat[7]*inmat[3]-inmat[6]*inmat[4])/det;
    outmat[7]=-(inmat[7]*inmat[0]-inmat[6]*inmat[1])/det;
    outmat[8]= (inmat[4]*inmat[0]-inmat[3]*inmat[1])/det;
  }
  if (determinant) *determinant=det;
  PetscFunctionReturn(0);
}


/* 2-D functions */

/*@
  Compute_Basis_2D - Compute the linear Lagrange basis functions and its derivatives
  on a linear triangular or quadrilateral element.

  Input Parameter:
. n - Number of vertices in the element
. verts - Array of coordinates of the vertices

  Output Parameter:
. quad - The quadrature points on the reference element
. phypts - The equivalent quadrature points on the physical element
. jxw - Product of Jacobian times the quadrature weight
. phi - Basis functions evaluated at the quadrature points
. dphidx - Derivative of basis functions wrt X evaluated at the quadrature points
. dphidy - Derivative of basis functions wrt Y evaluated at the quadrature points
@*/
#undef __FUNCT__
#define __FUNCT__ "Compute_Basis_2D"
PetscErrorCode Compute_Basis_2D ( PetscInt nverts, PetscReal *coords/*nverts*3*/, PetscInt npts, PetscReal *quad/*npts*3*/, PetscReal *phypts/*npts*3*/, 
        PetscReal *jxw/*npts*/, PetscReal *phi/*npts*/, PetscReal *dphidx/*npts*/, PetscReal *dphidy/*npts*/)
{
  int i,j;
  PetscReal jacobian[4],ijacobian[4];
  double jacobiandet;
  PetscErrorCode    ierr;
  PetscFunctionBegin;

  ierr = PetscMemzero(phypts,npts*3*sizeof(PetscReal));CHKERRQ(ierr);
  if (dphidx) { /* Reset arrays. */
    ierr = PetscMemzero(dphidx,npts*nverts*sizeof(PetscReal));CHKERRQ(ierr);
    ierr = PetscMemzero(dphidy,npts*nverts*sizeof(PetscReal));CHKERRQ(ierr);
  }
  if (nverts == 4) { /* Linear Quadrangle */

    /* 2-D 2-point tensor product Gaussian quadrature */
    quad[0]=-0.5773502691896257; quad[1]=-0.5773502691896257;
    quad[3]=-0.5773502691896257; quad[4]=0.5773502691896257;
    quad[6]=0.5773502691896257; quad[7]=-0.5773502691896257;
    quad[9]=0.5773502691896257; quad[10]=0.5773502691896257;
    /* transform quadrature bounds: [-1, 1] => [0, 1] */
    for (i=0; i<nverts*3; ++i) {
      quad[i]=0.5*quad[i]+0.5;
    }

    for (j=0;j<npts;j++)
    {
      const int offset=j*nverts;
      const double r = quad[0+j*3];
      const double s = quad[1+j*3];

      phi[0+offset] = ( 1.0 - r ) * ( 1.0 - s );
      phi[1+offset] =         r   * ( 1.0 - s );
      phi[2+offset] =         r   *         s;
      phi[3+offset] = ( 1.0 - r ) *         s;

      const double dNi_dxi[4]  = { -1.0 + s, 1.0 - s, s, -s };
      const double dNi_deta[4] = { -1.0 + r, -r, r, 1.0 - r };

      ierr = PetscMemzero(jacobian,4*sizeof(PetscReal));CHKERRQ(ierr);
      ierr = PetscMemzero(ijacobian,4*sizeof(PetscReal));CHKERRQ(ierr);
      for (i = 0; i < nverts; ++i) {
        const PetscScalar* vertices = coords+i*3;
        jacobian[0] += dNi_dxi[i] * vertices[0];
        jacobian[2] += dNi_dxi[i] * vertices[1];
        jacobian[1] += dNi_deta[i] * vertices[0];
        jacobian[3] += dNi_deta[i] * vertices[1];
        for (int k = 0; k < 3; ++k)
          phypts[3*j+k] += phi[i+offset] * vertices[k];
      }

      /* invert the jacobian */
      ierr = invert_mat_2x2(jacobian, ijacobian, &jacobiandet);CHKERRQ(ierr);

      jxw[j] = jacobiandet/(nverts);

      /*  Divide by element jacobian. */
      for ( i = 0; i < nverts; i++ ) {
        for (int k = 0; k < 2; ++k) {
          if (dphidx) dphidx[i+offset] += dNi_dxi[i]*ijacobian[k*2+0];
          if (dphidy) dphidy[i+offset] += dNi_deta[i]*ijacobian[k*2+1];
        }
      }

    }
  }
  else { /* Linear triangle */
    /* 2-D 3-point Gaussian quadrature */
    quad[0]=1.0/6; quad[1]=1.0/6;
    quad[3]=2.0/3; quad[4]=1.0/6;
    quad[6]=1.0/6; quad[7]=2.0/3;

    ierr = PetscMemzero(jacobian,4*sizeof(PetscReal));CHKERRQ(ierr);
    ierr = PetscMemzero(ijacobian,4*sizeof(PetscReal));CHKERRQ(ierr);

    /* Jacobian is constant */
    jacobian[0] = (coords[0*3+0]-coords[2*3+0]); jacobian[1] = (coords[1*3+0]-coords[2*3+0]);
    jacobian[2] = (coords[0*3+1]-coords[2*3+1]); jacobian[3] = (coords[1*3+1]-coords[2*3+1]);

    /* invert the jacobian */
    ierr = invert_mat_2x2(jacobian, ijacobian, &jacobiandet);CHKERRQ(ierr);
    if ( jacobiandet < 1e-8 ) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Triangular element has zero volume: %g. Degenerate element or invalid connectivity\n", jacobiandet);

    for (j=0;j<npts;j++)
    {
      const int offset=j*nverts;
      const double r = quad[0+j*3];
      const double s = quad[1+j*3];

      jxw[j] = jacobiandet/(nverts)/6;

      phi[0+offset] = r;
      phi[1+offset] = s;
      phi[2+offset] = 1.0 - r - s;

      if (dphidx) {
        dphidx[0+offset] = (coords[1*3+1]-coords[2*3+1])/jacobiandet;
        dphidx[1+offset] = (coords[2*3+1]-coords[0*3+1])/jacobiandet;
        dphidx[2+offset] = -dphidx[0+offset] - dphidx[1+offset];
      }

      if (dphidy) {
        dphidy[0+offset] = (coords[2*3+0]-coords[1*3+0])/jacobiandet;
        dphidy[1+offset] = (coords[0*3+0]-coords[2*3+0])/jacobiandet;
        dphidy[2+offset] = -dphidy[0+offset] - dphidy[1+offset];
      }

      for (i = 0; i < nverts; ++i) {
        const PetscScalar* vertices = coords+i*3;
        for (int k = 0; k < 3; ++k)
          phypts[3*j+k] += phi[i+offset] * vertices[k];
      }

    }
  }
#if 0
  /* verify if the computed basis functions are consistent */
  for ( j = 0; j < npts; j++ ) {
    PetscScalar phisum=0,dphixsum=0,dphiysum=0;
    for ( i = 0; i < nverts; i++ ) {
      phisum += phi[i+j*nverts];
      if (dphidx) dphixsum += dphidx[i+j*nverts];
      if (dphidy) dphiysum += dphidy[i+j*nverts];
    }
    PetscPrintf(PETSC_COMM_WORLD, "Sum of basis at quadrature point %D = %g, %g, %g\n", j, phisum, dphixsum, dphiysum);
  }
#endif
  PetscFunctionReturn(0);
}


/* 3-D functions */

/*
*  Purpose: Compute_Basis_3D: all bases at N points for a HEX8 element.
*
*  Discussion:
*
*    The routine is given the coordinates of the vertices of a hexahedra.
*    It works directly with these coordinates, and does not refer to a 
*    reference element.
*
*    The sides of the element are presumed to lie along coordinate axes.
*
*    The routine evaluates the basis functions associated with each corner,
*    and their derivatives with respect to X and Y.
*
*  Physical Element HEX8:
*
*      8------7        t  s
*     /|     /|        | /
*    5------6 |        |/
*    | |    | |        0-------r
*    | 4----|-3        
*    |/     |/        
*    1------2        
*     
*
*  Parameters:
*
*    Input, PetscScalar Q[3*8], the coordinates of the vertices.
*    It is common to list these points in counter clockwise order.
*
*    Input, int N, the number of evaluation points.
*
*    Input, PetscScalar P[3*N], the evaluation points.
*
*    Output, PetscScalar PHI[8*N], the bases at the evaluation points.
*
*    Output, PetscScalar DPHIDX[8*N], DPHIDY[8*N], DPHIDZ[8*N] the 
*    derivatives of the bases at the evaluation points.
*
*  Original Author: John Burkardt (http://people.sc.fsu.edu/~jburkardt/cpp_src/fem3d_pack/fem3d_pack.cpp)
*  Modified by Vijay Mahadevan
*/
#undef __FUNCT__
#define __FUNCT__ "Compute_Basis_3D"
PetscErrorCode Compute_Basis_3D ( PetscInt nverts, PetscReal *coords/*nverts*3*/, PetscInt npts, PetscReal *quad/*npts*3*/, PetscReal *phypts/*npts*3*/, 
        PetscReal *jxw/*npts*/, PetscReal *phi/*npts*/, PetscReal *dphidx/*npts*/, PetscReal *dphidy/*npts*/, PetscReal *dphidz/*npts*/)
{
  PetscReal volume;
  int i,j;
  PetscReal jacobian[9],ijacobian[9];
  PetscErrorCode ierr;

  PetscFunctionBegin;
  /* Reset arrays. */
  ierr = PetscMemzero(phi,npts*nverts*sizeof(PetscReal));CHKERRQ(ierr);
  ierr = PetscMemzero(quad,npts*3*sizeof(PetscReal));CHKERRQ(ierr);
  ierr = PetscMemzero(phypts,npts*3*sizeof(PetscReal));CHKERRQ(ierr);
  if (dphidx) {
    ierr = PetscMemzero(dphidx,npts*nverts*sizeof(PetscReal));CHKERRQ(ierr);
    ierr = PetscMemzero(dphidy,npts*nverts*sizeof(PetscReal));CHKERRQ(ierr);
    ierr = PetscMemzero(dphidz,npts*nverts*sizeof(PetscReal));CHKERRQ(ierr);
  }

  if (nverts == 8) { // Linear Hexahedra

    /* 3-D 2-point tensor product Gaussian quadrature */
    quad[0]=-0.5773502691896257; quad[1]=-0.5773502691896257;  quad[2]=-0.5773502691896257;
    quad[3]=-0.5773502691896257; quad[4]=0.5773502691896257;   quad[5]=-0.5773502691896257;
    quad[6]=0.5773502691896257;  quad[7]=-0.5773502691896257;  quad[8]=-0.5773502691896257;
    quad[9]=0.5773502691896257;  quad[10]=0.5773502691896257;  quad[11]=-0.5773502691896257;

    quad[12]=-0.5773502691896257; quad[13]=-0.5773502691896257;  quad[14]=0.5773502691896257;
    quad[15]=-0.5773502691896257; quad[16]=0.5773502691896257;   quad[17]=0.5773502691896257;
    quad[18]=0.5773502691896257;  quad[19]=-0.5773502691896257;  quad[20]=0.5773502691896257;
    quad[21]=0.5773502691896257;  quad[22]=0.5773502691896257;   quad[23]=0.5773502691896257;

    for (j=0;j<npts;j++)
    {
      const int offset = j*nverts;
      const double& r = quad[j*3+0];
      const double& s = quad[j*3+1];
      const double& t = quad[j*3+2];

      phi[offset+0] = ( 1.0 - r ) * ( 1.0 - s ) * ( 1.0 - t ) / 8;
      phi[offset+1] = ( 1.0 + r ) * ( 1.0 - s ) * ( 1.0 - t ) / 8;
      phi[offset+2] = ( 1.0 + r ) * ( 1.0 + s ) * ( 1.0 - t ) / 8;
      phi[offset+3] = ( 1.0 - r ) * ( 1.0 + s ) * ( 1.0 - t ) / 8;
      phi[offset+4] = ( 1.0 - r ) * ( 1.0 - s ) * ( 1.0 + t ) / 8;
      phi[offset+5] = ( 1.0 + r ) * ( 1.0 - s ) * ( 1.0 + t ) / 8;
      phi[offset+6] = ( 1.0 + r ) * ( 1.0 + s ) * ( 1.0 + t ) / 8;
      phi[offset+7] = ( 1.0 - r ) * ( 1.0 + s ) * ( 1.0 + t ) / 8;

      const double dNi_dxi[8]  = { - ( 1.0 - s ) * ( 1.0 - t ),
                                     ( 1.0 - s ) * ( 1.0 - t ),
                                     ( 1.0 + s ) * ( 1.0 - t ),
                                   - ( 1.0 + s ) * ( 1.0 - t ),
                                   - ( 1.0 - s ) * ( 1.0 + t ),
                                     ( 1.0 - s ) * ( 1.0 + t ),
                                     ( 1.0 + s ) * ( 1.0 + t ),
                                   - ( 1.0 + s ) * ( 1.0 + t )  };

      const double dNi_deta[8]  = { - ( 1.0 - r ) * ( 1.0 - t ),
                                    - ( 1.0 + r ) * ( 1.0 - t ),
                                      ( 1.0 + r ) * ( 1.0 - t ),
                                      ( 1.0 - r ) * ( 1.0 - t ),
                                    - ( 1.0 - r ) * ( 1.0 + t ),
                                    - ( 1.0 + r ) * ( 1.0 + t ),
                                      ( 1.0 + r ) * ( 1.0 + t ),
                                      ( 1.0 - r ) * ( 1.0 + t ) };

      const double dNi_dzeta[8]  = { - ( 1.0 - r ) * ( 1.0 - s ),
                                     - ( 1.0 + r ) * ( 1.0 - s ),
                                     - ( 1.0 + r ) * ( 1.0 + s ),
                                     - ( 1.0 - r ) * ( 1.0 + s ),
                                       ( 1.0 - r ) * ( 1.0 - s ),
                                       ( 1.0 + r ) * ( 1.0 - s ),
                                       ( 1.0 + r ) * ( 1.0 + s ),
                                       ( 1.0 - r ) * ( 1.0 + s ) };

      ierr = PetscMemzero(jacobian,9*sizeof(PetscReal));CHKERRQ(ierr);
      ierr = PetscMemzero(ijacobian,9*sizeof(PetscReal));CHKERRQ(ierr);
      double factor = 1.0/8;
      for (i = 0; i < nverts; ++i) {
        const PetscScalar* vertex = coords+i*3;
        jacobian[0] += dNi_dxi[i]   * vertex[0];
        jacobian[3] += dNi_dxi[i]   * vertex[1];
        jacobian[6] += dNi_dxi[i]   * vertex[2];
        jacobian[1] += dNi_deta[i]  * vertex[0];
        jacobian[4] += dNi_deta[i]  * vertex[1];
        jacobian[7] += dNi_deta[i]  * vertex[2];
        jacobian[2] += dNi_dzeta[i] * vertex[0];
        jacobian[5] += dNi_dzeta[i] * vertex[1];
        jacobian[8] += dNi_dzeta[i] * vertex[2];
      }

      /* invert the jacobian */
      ierr = invert_mat_3x3(jacobian, ijacobian, &volume);CHKERRQ(ierr);

      jxw[j] = factor*volume/(nverts);

      /*  Divide by element jacobian. */
      for ( i = 0; i < nverts; ++i ) {
        const PetscScalar* vertex = coords+i*3;
        for (int k = 0; k < 3; ++k) {
          phypts[3*j+k] += phi[i+offset] * vertex[k];
          if (dphidx) dphidx[i+offset] += dNi_dxi[i]   * ijacobian[0*3+k];
          if (dphidy) dphidy[i+offset] += dNi_deta[i]  * ijacobian[1*3+k];
          if (dphidz) dphidz[i+offset] += dNi_dzeta[i] * ijacobian[2*3+k];
        }
      }
    }
  }
  else
  {
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"The number of entity vertices are invalid. Currently only support HEX8: %D",nverts);
  }
#if 0
  /* verify if the computed basis functions are consistent */
  for ( j = 0; j < npts; j++ ) {
    PetscScalar phisum=0,dphixsum=0,dphiysum=0,dphizsum=0;
    const int offset = j*nverts;
    for ( i = 0; i < nverts; i++ ) {
      phisum += phi[i+offset];
      if (dphidx) dphixsum += dphidx[i+offset];
      if (dphidy) dphiysum += dphidy[i+offset];
      if (dphidz) dphizsum += dphidz[i+offset];
      if (dphidx) PetscPrintf(PETSC_COMM_WORLD, "\t Values [%d]: [JxW] [phi, dphidx, dphidy, dphidz] = %g, %g, %g, %g, %g\n", j, jxw[j], phi[i+offset], dphidx[i+offset], dphidy[i+offset], dphidz[i+offset]);
    }
    if (dphidx) PetscPrintf(PETSC_COMM_WORLD, "Sum of basis at quadrature point %D (%g, %g, %g) = %g, %g, %g, %g\n", j, quad[3*j+0], quad[3*j+1], quad[3*j+2], phisum, dphixsum, dphiysum, dphizsum);
  }
#endif
  PetscFunctionReturn(0);
}



