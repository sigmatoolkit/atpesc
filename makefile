#
# Makefile for compiling MOAB and DMMoab examples for ATPESC 2015 workshop
#
# MOAB_DIR points to top-level install dir, below which MOAB's lib/ and include/ are located
MOAB_DIR := /projects/FASTMath/ATPESC-2016/installs/moab/4.9.2/ppc64-bgq-rhel6-gcc4.4/optimized
# PETSC_DIR points to top-level install dir, below which PETSc's conf/, lib/ and include/ are located
PETSC_DIR := /projects/FASTMath/ATPESC-2016/installs/petsc/master-moab/ppc64-bgq-rhel6-gcc4.4
# prefix points to top-level examples dir, where the pre-compiled executables are available
prefix := /projects/FASTMath/ATPESC-2016/examples/moab

# MOAB includes
include ${MOAB_DIR}/lib/moab.make
include ${MOAB_DIR}/lib/iMesh-Defs.inc
# PETSc includes
include ${PETSC_DIR}/lib/petsc/conf/variables

.SUFFIXES: .o .cpp .cxx

# MESH_DIR is the directory containing mesh files that come with MOAB source
MESH_DIR=${PWD}/input

MOAB_BASIC_EXAMPLES = HelloParMOAB GenLargeMesh 
ADVANCED_EXAMPLES = MBSize LloydRelaxation
PETSC_EXAMPLES = DMMoabLaplacian2D DMMoabPoisson3D

default: ${MOAB_BASIC_EXAMPLES} ${PETSC_EXAMPLES}
advanced: ${ADVANCED_EXAMPLES}
all: default advanced

EXTRALIBS=-ldl

HelloParMOAB: HelloParMOAB.o ${MOAB_LIBDIR}/libMOAB.la
	${MOAB_CXX} -o $@ $< ${MOAB_LIBS_LINK} ${EXTRALIBS}

GenLargeMesh: GenLargeMesh.o ${MOAB_LIBDIR}/libMOAB.la
	${MOAB_CXX} -o $@ $< ${MOAB_LIBS_LINK} ${EXTRALIBS}

MBSize: MBSize.o ${MOAB_LIBDIR}/libMOAB.la
	${MOAB_CXX} -o $@ $< ${MOAB_LIBS_LINK} ${EXTRALIBS}

LloydRelaxation: LloydRelaxation.o ${MOAB_LIBDIR}/libMOAB.la
	${MOAB_CXX} -o $@ $< ${MOAB_LIBS_LINK} ${EXTRALIBS}

DMMoabLaplacian2D: DMMoabLaplacian2D.o FEMDiscretization.o ${PETSC_DIR}/include/petsc.h
	-${CLINKER} -o  $@ $< FEMDiscretization.o ${PETSC_LIB}

DMMoabPoisson3D: DMMoabPoisson3D.o FEMDiscretization.o ${PETSC_DIR}/include/petsc.h
	-${CLINKER} -o  $@ $< FEMDiscretization.o ${PETSC_LIB}

install:
	cp makefile          ${prefix}/
	cp HelloParMOAB      ${prefix}/
	cp GenLargeMesh      ${prefix}/
	cp DMMoabLaplacian2D ${prefix}/
	cp DMMoabPoisson3D   ${prefix}/
	cp LloydRelaxation   ${prefix}/
	cp MBSize            ${prefix}/
	cp -rf input/        ${prefix}/
	mkdir -p ${prefix}/output

clean::
	rm -rf *.o *.mod *.h5m ${MOAB_BASIC_EXAMPLES} ${PETSC_EXAMPLES} ${ADVANCED_EXAMPLES}

.cpp.o:
	${MOAB_CXX} ${CXXFLAGS} ${MOAB_CXXFLAGS} ${MOAB_CPPFLAGS} ${MOAB_INCLUDES} -DMESH_DIR=\"${MESH_DIR}\" -c $<

.cxx.o:
	${CXX} -c ${CXX_FLAGS} ${CXXFLAGS} ${CCPPFLAGS} -c -Wall $< -o $@
