/** @example LloydRelaxation.cpp \n
 * \brief Perform Lloyd relaxation on a mesh and its dual \n
 * <b>To run</b>: mpiexec -np <np> LloydRelaxation [filename]\n
 *
 * Briefly, Lloyd relaxation is a technique to smooth out a mesh.  The centroid of each cell is computed from its
 * vertex positions, then vertices are placed at the average of their connected cells' centroids.
 *
 * In the parallel algorithm, an extra ghost layer of cells is exchanged.  This allows us to compute the centroids
 * for boundary cells on each processor where they appear; this eliminates the need for one round of data exchange
 * (for those centroids) between processors.  New vertex positions must be sent from owning processors to processors
 * sharing those vertices.  Convergence is measured as the maximum distance moved by any vertex.  
 * 
 * In this implementation, a fixed number of iterations is performed.  The final mesh is output to 'lloydfinal.h5m'
 * in the current directory (H5M format must be used since the file is written in parallel).
 *
 * Usage: mpiexec -n 2 valgrind ./LloydRelaxation -f input/surfrandomtris-6128part.h5m -r 2 -p 2
 */

#include <iostream>
#include <sstream>
#include "moab/Core.hpp"
#ifdef MOAB_HAVE_MPI
#  include "moab/ParallelComm.hpp"
#  include "MBParallelConventions.h"
#endif
#include "moab/Skinner.hpp"
#include "moab/CN.hpp"
#include "moab/ProgOptions.hpp"
#include "moab/CartVect.hpp"
#include "moab/NestedRefine.hpp"
#include "MBExamples.hpp"

using namespace moab;
using namespace std;

#ifdef MESH_DIR
string test_file_name = string(MESH_DIR) + string("/surfrandomtris-128part.h5m");
#else
string test_file_name = string("input/surfrandomtris-128part.h5m");
#endif

#define RC if (MB_SUCCESS != rval) return rval

ErrorCode perform_lloyd_relaxation(Interface *mb, Range &verts, Range &cells, Tag fixed, 
                                   int num_its, double rel_eps, int report_its);

int main(int argc, char **argv)
{
  int num_its = 10;
  int num_ref = 1;
  int num_dim = 2;
  int report_its = 1;
  int num_degree = 2;
  double rel_eps = 1e-5;
  const int nghostrings = 1;
  ProgOptions opts;
  ErrorCode rval;
  std::stringstream sstr;
  int global_rank;
  
  MPI_Init(&argc, &argv);
  MPI_Comm_rank( MPI_COMM_WORLD, &global_rank );

  // Decipher program options from user
  opts.addOpt<int>(std::string("niter,n"),
      std::string("Number of Lloyd smoothing iterations (default=10)"), &num_its);
  opts.addOpt<double>(std::string("eps,e"),
      std::string("Tolerance for the Lloyd smoothing error (default=1e-5)"), &rel_eps);
  opts.addOpt<int>(std::string("dim,d"),
      std::string("Topological dimension of the mesh (default=2)"), &num_dim);
  opts.addOpt<std::string>(std::string("file,f"),
      std::string("Input mesh file to smoothen (default=input/surfrandomtris-128part.h5m)"), &test_file_name);
  opts.addOpt<int>(std::string("nrefine,r"),
      std::string("Number of uniform refinements to perform and apply smoothing cycles (default=1)"), &num_ref);
  opts.addOpt<int>(std::string("ndegree,p"),
      std::string("Degree of uniform refinement (default=2)"), &num_degree);

  opts.parseCommandLine(argc, argv);

  // get MOAB and ParallelComm instances
  Core *mb = new Core;
  if (NULL == mb) return 1;
  int global_size = 1;

  // get the ParallelComm instance
  ParallelComm *pcomm = new ParallelComm(mb, MPI_COMM_WORLD);
  global_size = pcomm->size();

  string roptions,woptions;
  if (global_size > 1) { // if reading in parallel, need to tell it how
    sstr.str("");
    sstr << "PARALLEL=READ_PART;PARTITION=PARALLEL_PARTITION;PARALLEL_RESOLVE_SHARED_ENTS;PARALLEL_GHOSTS=" 
         << num_dim << ".0." << nghostrings
         << ";DEBUG_IO=0;DEBUG_PIO=0";
    roptions = sstr.str();
    woptions = "PARALLEL=WRITE_PART";
  }

  // read the file
  moab::EntityHandle fileset,currset;
  rval = mb->create_meshset(MESHSET_SET, fileset); RC;
  currset = fileset;
  rval = mb->load_file(test_file_name.c_str(), &fileset, roptions.c_str()); RC;

  std::vector<EntityHandle> hsets(num_ref+1,fileset);
  if (num_ref) {
    // Perform uniform refinement of the smoothed mesh
#ifdef MOAB_HAVE_MPI
    NestedRefine *uref = new NestedRefine(mb, pcomm, currset);
#else
    NestedRefine *uref = new NestedRefine(mb, 0, currset);
#endif

    std::vector<int> num_degrees(num_ref, num_degree);
    rval = uref->generate_mesh_hierarchy(num_ref, &num_degrees[0], hsets); RC;

    // Now exchange 1 layer of ghost elements, using vertices as bridge
    // (we could have done this as part of reading process, using the PARALLEL_GHOSTS read option)
    rval = uref->exchange_ghosts(hsets, nghostrings); RC;

    delete uref;
  }

  for (int iref=0; iref <= num_ref; ++iref) {

    // specify which set we are currently working on
    currset = hsets[iref];

    // make tag to specify fixed vertices, since it's input to the algorithm; use a default value of non-fixed
    // so we only need to set the fixed tag for skin vertices
    Tag fixed;
    int def_val = 0;
    rval = mb->tag_get_handle("fixed", 1, MB_TYPE_INTEGER, fixed, MB_TAG_CREAT | MB_TAG_DENSE, &def_val); RC;

    // get all vertices and faces
    Range verts, faces, skin_verts;
    rval = mb->get_entities_by_type(currset, MBVERTEX, verts); RC;
    rval = mb->get_entities_by_dimension(currset, num_dim, faces); RC;
    dbgprint ( "Found " << verts.size() << " vertices and " << faces.size() << " elements" );
    
    // get the skin vertices of those faces and mark them as fixed; we don't want to fix the vertices on a
    // part boundary, but since we exchanged a layer of ghost faces, those vertices aren't on the skin locally
    // ok to mark non-owned skin vertices too, I won't move those anyway
    // use MOAB's skinner class to find the skin
    Skinner skinner(mb);
    rval = skinner.find_skin(currset, faces, true, skin_verts); RC; // 'true' param indicates we want vertices back, not faces

    std::vector<int> fix_tag(skin_verts.size(), 1); // initialized to 1 to indicate fixed
    rval = mb->tag_set_data(fixed, skin_verts, &fix_tag[0]); RC;
    // exchange tags on owned verts for fixed points
    if (global_size > 1) {
#ifdef MOAB_HAVE_MPI
      rval = pcomm->exchange_tags(fixed, skin_verts); RC;
#endif
    }

    // now perform the Lloyd relaxation
    rval = perform_lloyd_relaxation(mb, verts, faces, fixed, num_its, rel_eps, report_its); RC;
    
    // output file, using parallel write
    sstr.str("");
    sstr << "output/LloydSmoother_" << iref << ".h5m";
    rval = mb->write_file(sstr.str().c_str(), "H5M", woptions.c_str(), &currset, 1); RC;

    // delete fixed tag, since we created it here
    rval = mb->tag_delete(fixed); RC;
  }

  delete pcomm;
  // delete MOAB instance
  delete mb;
  
  MPI_Finalize();
  return 0;
}

ErrorCode perform_lloyd_relaxation(Interface *mb, Range &verts, Range &faces, Tag fixed, 
                                   int num_its, double rel_eps, int report_its) 
{
  ErrorCode rval;
  int global_rank = 0, global_size = 1;

#ifdef MOAB_HAVE_MPI
  ParallelComm *pcomm = ParallelComm::get_pcomm(mb, 0);
  global_rank = pcomm->rank();
  global_size = pcomm->size();
#endif
  
  dbgprint ( "-- Starting smoothing cycle --" );
    // perform Lloyd relaxation:
    // 1. setup: set vertex centroids from vertex coords; filter to owned verts; get fixed tags

    // get all verts coords into tag; don't need to worry about filtering out fixed verts, 
    // we'll just be setting to their fixed coords
  std::vector<double> vcentroids(3*verts.size());
  rval = mb->get_coords(verts, &vcentroids[0]); RC;

  Tag centroid;
  rval = mb->tag_get_handle("centroid", 3, MB_TYPE_DOUBLE, centroid, MB_TAG_CREAT | MB_TAG_DENSE); RC;
  rval = mb->tag_set_data(centroid, verts, &vcentroids[0]); RC;

    // filter verts down to owned ones and get fixed tag for them
  Range owned_verts, shared_owned_verts;
  if (global_size > 1) {
#ifdef MOAB_HAVE_MPI
    rval = pcomm->filter_pstatus(verts, PSTATUS_NOT_OWNED, PSTATUS_NOT, -1, &owned_verts);
    if (rval != MB_SUCCESS) return rval;
#endif
  }
  else
    owned_verts = verts;
  std::vector<int> fix_tag(owned_verts.size());
  rval = mb->tag_get_data(fixed, owned_verts, &fix_tag[0]); RC;

    // now fill vcentroids array with positions of just owned vertices, since those are the ones
    // we're actually computing
  vcentroids.resize(3*owned_verts.size());
  rval = mb->tag_get_data(centroid, owned_verts, &vcentroids[0]); RC;

#ifdef MOAB_HAVE_MPI
    // get shared owned verts, for exchanging tags
  rval = pcomm->get_shared_entities(-1, shared_owned_verts, 0, false, true); RC;
    // workaround: if no shared owned verts, put a non-shared one in the list, to prevent exchanging tags
    // for all shared entities
  if (shared_owned_verts.empty()) shared_owned_verts.insert(*verts.begin());
#endif
  
    // some declarations for later iterations
  std::vector<double> fcentroids(3*faces.size()); // fcentroids for face centroids
  std::vector<double> ctag(3*CN::MAX_NODES_PER_ELEMENT);  // temporary coordinate storage for verts bounding a face
  const EntityHandle *conn;  // const ptr & size to face connectivity
  int nconn;
  Range::iterator fit, vit;  // for iterating over faces, verts
  int f, v;  // for indexing into centroid vectors
  std::vector<EntityHandle> adj_faces;  // used in vertex iteration

  double mxdelta = 0.0, global_max;
    // 2. for num_its iterations:
  for (int nit = 0; nit < num_its; nit++) {
    
    mxdelta = 0.0;

    // 2a. foreach face: centroid = sum(vertex centroids)/num_verts_in_cell
    for (fit = faces.begin(), f = 0; fit != faces.end(); fit++, f++) {
        // get verts for this face
      rval = mb->get_connectivity(*fit, conn, nconn); RC;
        // get centroid tags for those verts
      rval = mb->tag_get_data(centroid, conn, nconn, &ctag[0]); RC;
      fcentroids[3*f+0] = fcentroids[3*f+1] = fcentroids[3*f+2] = 0.0;
      for (v = 0; v < nconn; v++) {
        fcentroids[3*f+0] += ctag[3*v+0];
        fcentroids[3*f+1] += ctag[3*v+1];
        fcentroids[3*f+2] += ctag[3*v+2];
      }
      for (v = 0; v < 3; v++) fcentroids[3*f+v] /= nconn;
    }
    rval = mb->tag_set_data(centroid, faces, &fcentroids[0]); RC;

      // 2b. foreach owned vertex: 
    for (vit = owned_verts.begin(), v = 0; vit != owned_verts.end(); vit++, v++) {
        // if !fixed
      if (fix_tag[v]) continue;
        // vertex centroid = sum(cell centroids)/ncells
      adj_faces.clear();
      rval = mb->get_adjacencies(&(*vit), 1, 2, false, adj_faces); RC;
      rval = mb->tag_get_data(centroid, &adj_faces[0], adj_faces.size(), &fcentroids[0]); RC;
      double vnew[] = {0.0, 0.0, 0.0};
      for (f = 0; f < (int)adj_faces.size(); f++) {
        vnew[0] += fcentroids[3*f+0];
        vnew[1] += fcentroids[3*f+1];
        vnew[2] += fcentroids[3*f+2];
      }
      for (f = 0; f < 3; f++) vnew[f] /= adj_faces.size();
      double delta = (CartVect(vnew)-CartVect(&vcentroids[3*v])).length();
      mxdelta = std::max(delta, mxdelta);
      for (f = 0; f < 3; f++) vcentroids[3*v+f] = vnew[f];
    }

      // set the centroid tag; having them only in vcentroids array isn't enough, as vertex centroids are
      // accessed randomly in loop over faces
    rval = mb->tag_set_data(centroid, owned_verts, &vcentroids[0]); RC;

    // 2c. exchange tags on owned verts
    if (global_size > 1) {
#ifdef MOAB_HAVE_MPI
      rval = pcomm->exchange_tags(centroid, shared_owned_verts); RC;
#endif
    }

    // global reduce for maximum delta, then report it
    global_max = mxdelta;
#ifdef MOAB_HAVE_MPI
    if (global_size > 1)
      MPI_Allreduce(&mxdelta, &global_max, 1, MPI_DOUBLE, MPI_MAX, pcomm->comm());
#endif

    if (!(nit%report_its)) {
      dbgprint( "\tIterate " << nit << ": Max delta = " << global_max << "." );
    }

    if (global_max < rel_eps) break;
  }
  
    // write the tag back onto vertex coordinates
  rval = mb->set_coords(owned_verts, &vcentroids[0]); RC;

    // delete the centroid tag, since we don't need it anymore
  rval = mb->tag_delete(centroid); RC;

  dbgprint( "-- Final iterate error = " << global_max << ".\n" );
  return MB_SUCCESS;
}
