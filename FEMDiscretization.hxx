/*
 * =====================================================================================
 *
 *       Filename:  FEMDiscretization.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/14/2016 02:07:41 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Vijay S. Mahadevan (vijaysm), mahadevan@anl.gov
 *        Company:  Argonne National Lab
 *
 * =====================================================================================
 */

#include <petsc.h>
#include "MBExamples.hpp"

PetscErrorCode Compute_Basis_2D ( PetscInt nverts, PetscReal *coords/*nverts*3*/, PetscInt npts, PetscReal *quad/*npts*3*/, PetscReal *phypts/*npts*3*/, 
        PetscReal *jxw/*npts*/, PetscReal *phi/*npts*/, PetscReal *dphidx/*npts*/, PetscReal *dphidy/*npts*/);

PetscErrorCode Compute_Basis_3D ( PetscInt nverts, PetscReal *coords/*nverts*3*/, PetscInt npts, PetscReal *quad/*npts*3*/, PetscReal *phypts/*npts*3*/, 
        PetscReal *jxw/*npts*/, PetscReal *phi/*npts*/, PetscReal *dphidx/*npts*/, PetscReal *dphidy/*npts*/, PetscReal *dphidz/*npts*/);

